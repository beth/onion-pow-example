# Onion Proof-of-Work Example

This is a simple Onion Service to illustrate proof-of-work (PoW) client puzzle
support in tor. The example consists of two components:

 1. A tor daemon, acting as a PoW-capable Onion Service (aka hidden service) and
    a router.
 2. A Python process that
   * Is attached to the tor daemon's control and metrics ports.
   * Serves as the HTTP Onion Service backend.
   * Has a load generator that creates traffic to the Onion Service using the
     same tor daemon, which is able to solve PoW client puzzles.

The tor daemon is configured to use a quite low dequeue rate limit, simulating
a DoS attack on a much smaller scale that's easy to generate on one machine
without impacting the network much. The Python process will continuously check
the size of the request backlog on the service, and adjust the amount of
generated load in order to keep the service backlogged at all times.

For every HTTP request, we can determine the corresponding Tor circuit and
show some stats about the circuit itself and the client's proof of work
solution if any.

Once the test stabilizes for a while, it's expected that the pqueue stays
non-empty and the suggested effort stays nonzero. A client may make it through
the request queue by chance even if it doesn't offer a PoW solution, but this
is marked as an inconclusive result. A successful result is any request that
we can link with a verified solution.

![Example screenshot](example.png)

## Usage

You can try this example in a number of ways:

 1. By running `run-in-docker.sh`, which requires Docker to be installed and
    available to your user (easier option).
 2. Bun manually installing a C Tor-capable binary somewhere on your system,
    and then running `run.sh` (or `TOR=/path/to/the/tor/binary ./run.sh`).
 3. Or separately run tor with the included `torrc` and start `service.py` with
    the same config file.

## Behavior

Startup will take a while, and you should expect to see a few things happen in
order:

 1. The tor daemon should start up
 2. On the console you'll see a `http://____.onion` address that will soon be
    active. Save it, and copy it to any clients under test (PoW-capable or
    not).
 3. The tor daemon will bootstrap, and upload HSDir descriptors
 4. You will start to see the load generator connecting, and issuing
    `GET /load` requests
 5. You should see periodic `[metrics]` lines indicating the queue depth and
    suggested effort. These will first be zero, but then you'll see the pqueue
    count increasing after several seconds and suggested_effort will increase
    after several minutes.
 6. At this point it's useful to try testing a client, by visiting the URL
    from earlier. All visits will be marked with the client's proof of work
    support level, or "inconclusive" if the test needs additional time.
 7. When retrying, request a "New Tor circuit for this site"

## License

Public Domain, no rights reserved.
