FROM debian:unstable
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
	bash \
        python3 \
        python3-qrcode \
        python3-requests \
        python3-stem \
        tmux \
        tor

# Verify that the tor package supports proof-of-work
RUN tor --list-modules | grep -q 'pow: yes'

RUN useradd -m user && chsh -s /bin/bash user
WORKDIR /home/user
USER user

COPY . .
CMD ./run.sh
